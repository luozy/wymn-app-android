/**
 * 
 */
package com.xiaoyun.org.app.adapter;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

/**
 * 通用的ViewHolder
 * 
 * @author yuanxy
 * 
 */
public class ViewHolder {
	private final SparseArray<View> mViews;
	private int mPosition;
	private View mConvertView;
	// private static ImageScaleType imageScaleType;
	private static DisplayImageOptions options;
	private ImageLoadingListener animateFirstListener = new AnimateFirstDisplayListener();

	private ViewHolder(Context context, ViewGroup parent, int layoutId,
			int position, DisplayImageOptions options) {
		this.mPosition = position;
		this.mViews = new SparseArray<View>();
		mConvertView = LayoutInflater.from(context).inflate(layoutId, parent,
				false);

		this.options = options;

		/*
		 * options = new DisplayImageOptions.Builder() //
		 * .showImageOnFail(R.drawable.ic_error)
		 * .cacheInMemory(true).cacheOnDisk(true).considerExifParams(true)
		 * .imageScaleType(imageScaleType != null ? imageScaleType :
		 * ImageScaleType.EXACTLY_STRETCHED)
		 * .delayBeforeLoading(10).displayer(new SimpleBitmapDisplayer())//
		 * //正常显示一张图片　 .build();
		 */

		// setTag
		mConvertView.setTag(this);
	}

	/**
	 * 拿到一个ViewHolder对象
	 * 
	 * @param context
	 * @param convertView
	 * @param parent
	 * @param layoutId
	 * @param position
	 * @return
	 */
	public static ViewHolder get(Context context, View convertView,
			ViewGroup parent, int layoutId, int position) {
		if (convertView == null) {
			return new ViewHolder(context, parent, layoutId, position, options);
		}
		return (ViewHolder) convertView.getTag();
	}

	/**
	 * 拿到一个ViewHolder对象
	 * 
	 * @param context
	 * @param convertView
	 * @param parent
	 * @param layoutId
	 * @param position
	 * @return
	 */
	public static ViewHolder get(Context context, View convertView,
			ViewGroup parent, int layoutId, int position,
			DisplayImageOptions options) {
		if (convertView == null) {
			return new ViewHolder(context, parent, layoutId, position, options);
		}
		return (ViewHolder) convertView.getTag();
	}

	public static ViewHolder get(Context context, View convertView,
			ViewGroup parent, int layoutId, int position,
			DisplayImageOptions options, ProgressBar spinner) {
		if (convertView == null) {
			return new ViewHolder(context, parent, layoutId, position, options);
		}
		return (ViewHolder) convertView.getTag();
	}

	public View getConvertView() {
		return mConvertView;
	}

	/**
	 * 通过控件的Id获取对于的控件，如果没有则加入views
	 * 
	 * @param viewId
	 * @return
	 */
	public <T extends View> T getView(int viewId) {
		View view = mViews.get(viewId);
		if (view == null) {
			view = mConvertView.findViewById(viewId);
			mViews.put(viewId, view);
		}
		return (T) view;
	}

	/**
	 * 为TextView设置字符串
	 * 
	 * @param viewId
	 * @param text
	 * @return
	 */
	public ViewHolder setText(int viewId, String text) {
		TextView view = getView(viewId);
		view.setText(text);
		return this;
	}

	/**
	 * 为ImageView设置图片
	 * 
	 * @param viewId
	 * @param drawableId
	 * @return
	 */
	public ViewHolder setImageResource(int viewId, int drawableId) {
		ImageView view = getView(viewId);
		view.setImageResource(drawableId);
		return this;
	}

	/**
	 * 为ImageView设置图片
	 * 
	 * @param viewId
	 * @param Bitmap
	 * @return
	 */
	public ViewHolder setImageBitmap(int viewId, Bitmap bm) {
		ImageView view = getView(viewId);
		view.setImageBitmap(bm);
		return this;
	}

	public int getPosition() {
		return mPosition;
	}

	/**
	 * 使用ImageLoader设置网络图片
	 * 
	 * @param viewId
	 * @param imageUrl
	 */
	public void displayImages(int viewId, String imageUrl) {
		ImageView imageView = getView(viewId);
		if ("null".equals(imageUrl)) {
			imageUrl = "http://h.hiphotos.baidu.com/image/pic/item/4bed2e738bd4b31c4859e0ba85d6277f9e2ff84e.jpg";
		}
		ImageLoader.getInstance().displayImage(imageUrl, imageView, options,
				animateFirstListener);
	}

	/**
	 * 停止图片缓存
	 */
	public void stopAndclear() {
		ImageLoader.getInstance().stop();
		AnimateFirstDisplayListener.displayedImages.clear();
	}

	private static class AnimateFirstDisplayListener extends
			SimpleImageLoadingListener {

		static final List<String> displayedImages = Collections
				.synchronizedList(new LinkedList<String>());

		@Override
		public void onLoadingComplete(String imageUri, View view,
				Bitmap loadedImage) {
			if (loadedImage != null) {
				ImageView imageView = (ImageView) view;
				boolean firstDisplay = !displayedImages.contains(imageUri);
				if (firstDisplay) {
					FadeInBitmapDisplayer.animate(imageView, 500);
					displayedImages.add(imageUri);
				}
			}
		}
	}

}
