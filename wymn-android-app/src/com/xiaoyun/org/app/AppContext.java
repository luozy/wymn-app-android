package com.xiaoyun.org.app;

import java.io.File;
import java.util.Properties;
import java.util.UUID;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Application;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Handler;
import android.os.Message;

import com.nostra13.universalimageloader.cache.disc.impl.UnlimitedDiscCache;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.StorageUtils;
import com.xiaoyun.org.R;
import com.xiaoyun.org.util.StringUtils;

/**
 * 全局应用程序类：用于保存和调用全�?��用配�?br>
 * 
 * @author yuanxy
 * @version 1.0
 * 
 */
public class AppContext extends Application {

	public static final int NETTYPE_WIFI = 0x01;
	public static final int NETTYPE_CMWAP = 0x02;
	public static final int NETTYPE_CMNET = 0x03;

	private boolean login = false; // 登录状�?
	private int loginUid = 0; // 登录用户的id

	private static String saveImagePath;// 保存图片路径

	public static int mNetWorkState = 0;// 网络状�?(没网)

	private Handler unLoginHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (msg.what == 1) {
				// UIHelper.ToastMessage(AppContext.this,
				// getString(R.string.msg_login_error));
				// UIHelper.showLoginDialog(AppContext.this);
			}
		}
	};

	@SuppressLint("NewApi")
	@TargetApi(Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onCreate() {

		init();

		super.onCreate();
	}

	/**
	 * 初始�?
	 */
	private void init() {
		// 设置保存图片的路
		// 暂时先这样，后续再调�? saveImagePath = getProperty(AppConfig.SAVE_IMAGE_PATH);
		if (StringUtils.isEmpty(saveImagePath)) {
			setProperty(AppConfig.SAVE_IMAGE_PATH,
					AppConfig.DEFAULT_SAVE_IMAGE_PATH);
			saveImagePath = AppConfig.DEFAULT_SAVE_IMAGE_PATH;
		}

		initImageLoader(getApplicationContext());

	}

	/**
	 * 初始�?ImageLoaderConfiguration
	 * 
	 * @param context
	 */
	public static void initImageLoader(Context context) {
		/*
		 * ImageLoaderConfiguration config = new
		 * ImageLoaderConfiguration.Builder(context)
		 * .threadPriority(Thread.NORM_PRIORITY - 2)
		 * .denyCacheImageMultipleSizesInMemory()
		 * .diskCacheFileNameGenerator(new Md5FileNameGenerator())
		 * .diskCacheSize(50 * 1024 * 1024) // 50 Mb
		 * .tasksProcessingOrder(QueueProcessingType.LIFO) .writeDebugLogs() //
		 * Remove for release app .build();
		 */
		// File cacheDir = new File(saveImagePath);
		File cacheDir = StorageUtils.getCacheDirectory(context);

		ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
				context)
				.threadPriority(Thread.NORM_PRIORITY - 2)
				// default
				.tasksProcessingOrder(QueueProcessingType.FIFO)
				// default
				.denyCacheImageMultipleSizesInMemory()
				.diskCache(new UnlimitedDiscCache(cacheDir))
				// 自定义缓存目�?
				.diskCacheSize(20 * 1024 * 1024).diskCacheFileCount(100)
				.diskCacheFileNameGenerator(new Md5FileNameGenerator()) // default
				.writeDebugLogs().build();

		ImageLoader.getInstance().init(config);
	}

	/**
	 * 网络是否可用
	 * 
	 * @return
	 */
	public boolean isNetworkConnected() {
		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo ni = cm.getActiveNetworkInfo();
		return ni != null && ni.isConnectedOrConnecting();
	}

	/**
	 * 获取当前网络类型
	 * 
	 * @return 0：没有网 1：WIFI网络 2：WAP网络 3：NET网络
	 */
	public int getNetworkType() {
		int netType = 0;
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
		if (networkInfo == null) {
			return netType;
		}
		int nType = networkInfo.getType();
		if (nType == ConnectivityManager.TYPE_MOBILE) {
			String extraInfo = networkInfo.getExtraInfo();
			if (!StringUtils.isEmpty(extraInfo)) {
				if (extraInfo.toLowerCase().equals("cmnet")) {
					netType = NETTYPE_CMNET;
				} else {
					netType = NETTYPE_CMWAP;
				}
			}
		} else if (nType == ConnectivityManager.TYPE_WIFI) {
			netType = NETTYPE_WIFI;
		}
		return netType;
	}

	/**
	 * 打开网络设置
	 * 
	 * @param context
	 */
	public void netWorkSettings(Context context) {
		Intent intent = null;
		// 判断手机系统的版�?即API大于10 就是3.0或以上版�?
		if (android.os.Build.VERSION.SDK_INT > 10) {
			intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
			// 这里打开系统设置
			// android.provider.Settings.ACTION_SETTINGS

			// 这里�?.0版本直接打开了移动网络设�?
			// android.provider.Settings.ACTION_WIRELESS_SETTINGS
		} else {
			intent = new Intent();
			ComponentName component = new ComponentName("com.android.settings",
					"com.android.settings.WirelessSettings");
			intent.setComponent(component);
			intent.setAction("android.intent.action.VIEW");
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		}
		context.startActivity(intent);
	}

	/**
	 * 判断当前版本是否兼容目标版本的方�? *
	 * 
	 * @param VersionCode
	 * 
	 * @return
	 */
	public static boolean isMethodsCompat(int VersionCode) {
		int currentVersion = android.os.Build.VERSION.SDK_INT;
		return currentVersion >= VersionCode;
	}

	/**
	 * 获取App安装包信�? *
	 * 
	 * @return
	 */
	public PackageInfo getPackageInfo() {
		PackageInfo info = null;
		try {
			info = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			e.printStackTrace(System.err);
		}
		if (info == null)
			info = new PackageInfo();
		return info;
	}

	/**
	 * 获取应用名称
	 * 
	 * @param context
	 * @return
	 */
	public static String getAppName(Context context) {
		String verName = context.getResources().getText(R.string.app_name)
				.toString();
		return verName;
	}

	/**
	 * 获取App唯一标识
	 * 
	 * @return
	 */
	public String getAppId() {
		String uniqueID = getProperty(AppConfig.CONF_APP_UNIQUEID);
		if (StringUtils.isEmpty(uniqueID)) {
			uniqueID = UUID.randomUUID().toString();
			setProperty(AppConfig.CONF_APP_UNIQUEID, uniqueID);
		}
		return uniqueID;
	}

	/**
	 * 用户是否登录
	 * 
	 * @return
	 */
	public boolean isLogin() {
		return login;
	}

	/**
	 * 获取登录用户id
	 * 
	 * @return
	 */
	public int getLoginUid() {
		return this.loginUid;
	}

	/**
	 * 用户注销
	 */
	public void Logout() {
		this.login = false;
		this.loginUid = 0;
	}

	/**
	 * 未登录或修改密码后的处理
	 */
	public Handler getUnLoginHandler() {
		return this.unLoginHandler;
	}

	/**
	 * 清除登录信息
	 */
	public void cleanLoginInfo(String loginType) {
		// this.loginUid = 0;
		// this.login = false;
		removeProperty("login", "loginType", "birthday", "sex", "pictureUrl",
				"screen_name", "profession", "userId", "corporeity",
				"attentionIllness", "maritalStatus", "user_uid",
				"profile_image_url", "gender", "token", "sid", "isAutoLogin",
				"getUserInfoDetails", "qqbdState", "wxbdState", "wbbdState");
		// 清除微博 Token信息�?
		if (loginType != null && "weiBo".equals(loginType)) {
			clear(getApplicationContext());
		}

	}

	/***
	 * 摇一摇是否发出声�? *
	 * 
	 * @return
	 */
	public boolean setSound() {
		if (!StringUtils.isEmpty(getProperty("isSound"))) {
			if (getProperty("isSound").equals("true")) {
				return true;
			} else {
				return false;
			}
		}
		return true;
	}

	// 以下用来保存数据的对应操�?
	public boolean containsProperty(String key) {
		Properties props = getProperties();
		return props.containsKey(key);
	}

	public void setProperties(Properties ps) {
		AppConfig.getAppConfig(this).set(ps);
	}

	public Properties getProperties() {
		return AppConfig.getAppConfig(this).get();
	}

	public void setProperty(String key, String value) {
		AppConfig.getAppConfig(this).set(key, value);
	}

	public String getProperty(String key) {
		return AppConfig.getAppConfig(this).get(key);
	}

	public void removeProperty(String... key) {
		AppConfig.getAppConfig(this).remove(key);
	}

	/**
	 * 清空 SharedPreferences �?Token信息�? *
	 * 
	 * @param context
	 *            应用程序上下文环�?
	 */
	public static void clear(Context context) {
		if (null == context) {
			return;
		}

		SharedPreferences pref = context.getSharedPreferences(
				AppConfig.PREFERENCES_NAME, Context.MODE_APPEND);
		Editor editor = pref.edit();
		editor.clear();
		editor.commit();
	}

}
