/*
 * 参�?�?liux的部分实�?(http://my.oschina.net/liux)
 */
package com.xiaoyun.org.app;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Environment;
import android.preference.PreferenceManager;

/**
 * 应用程序配置类：用于保存用户相关信息及设�? * 
 * @author yuanxy
 * @version 1.0
 */
@SuppressLint("NewApi")
public class AppConfig {

	private final static String APP_CONFIG = "config";
	public final static String CONF_APP_UNIQUEID = "APP_UNIQUEID";
	public final static String SAVE_IMAGE_PATH = "save_image_path";

	/** 当前 DEMO 应用�?APP_KEY，第三方应用应该使用自己�?APP_KEY 替换�?APP_KEY */
	public static final String APP_KEY = "276148951";
	// public static final String APP_SECRET =
	// "52093078e0a7fb63cecf514f1661a934";

	/**
	 * 当前 DEMO 应用的回调页，第三方应用可以使用自己的回调页�?	 * 
	 * <p>
	 * 注：关于授权回调页对移动客户端应用来说对用户是不可见的，�?��定义为何种形式都将不影响�?但是没有定义将无法使�?SDK 认证登录�?	 * 建议使用默认回调页：https://api.weibo.com/oauth2/default.html
	 * </p>
	 */
	public static final String REDIRECT_URL = "https://api.weibo.com/oauth2/default.html";
	public static final String SCOPE = "email,follow_app_official_microblog";

	/**
	 * Scope �?OAuth2.0 授权机制�?authorize 接口的一个参数�?通过 Scope，平台将�?��更多的微�?	 * 核心功能给开发�?，同时也加强用户隐私保护，提升了用户体验，用户在�?OAuth2.0 授权页中有权�?选择赋予应用的功能�?
	 * 
	 * 我们通过新浪微博�?��平台-->管理中心-->我的应用-->接口管理处，能看到我们目前已有哪些接口的 使用权限，高级权限需要进行申请�?
	 * 
	 * 目前 Scope 支持传入多个 Scope 权限，用逗号分隔�?	 * 
	 * 有关哪些 OpenAPI �?��权限申请，请查看：http://open.weibo.com/wiki/%E5%BE%AE%E5%8D%9AAPI
	 * 关于 Scope 概念及注意事项，请查看：http://open.weibo.com/wiki/Scope
	 */
	// public static final String SCOPE =
	// "email,direct_messages_read,direct_messages_write,"
	// + "friendships_groups_read,friendships_groups_write,statuses_to_me_read,"
	// + "follow_app_official_microblog," + "invitation_write";

	public static final String PREFERENCES_NAME = "com.hzkj.mylife";

	public static final String KEY_UID = "uid";
	public static final String KEY_ACCESS_TOKEN = "access_token";
	public static final String KEY_EXPIRES_IN = "expires_in";

	@SuppressLint("NewApi")
	public final static String DEFAULT_SAVE_IMAGE_PATH = Environment
			.getExternalStorageDirectory()
			+ File.separator
			+ "Mylife"
			+ File.separator;
	

	private Context mContext;
	private static AppConfig appConfig;
	//public static final String UPDATE_SAVENAME = "updateApp";

	public static AppConfig getAppConfig(Context context) {
		if (appConfig == null) {
			appConfig = new AppConfig();
			appConfig.mContext = context;
		}
		return appConfig;
	}

	/**
	 * 获取Preference设置
	 */
	public static SharedPreferences getSharedPreferences(Context context) {
		return PreferenceManager.getDefaultSharedPreferences(context);
	}

	public String get(String key) {
		Properties props = get();
		return (props != null) ? props.getProperty(key) : null;
	}

	public Properties get() {
		FileInputStream fis = null;
		Properties props = new Properties();
		try {
			// 读取files目录下的config
			// fis = activity.openFileInput(APP_CONFIG);

			// 读取app_config目录下的config
			File dirConf = mContext.getDir(APP_CONFIG, Context.MODE_PRIVATE);
			fis = new FileInputStream(dirConf.getPath() + File.separator
					+ APP_CONFIG);

			props.load(fis);
		} catch (Exception e) {
		} finally {
			try {
				fis.close();
			} catch (Exception e) {
			}
		}
		return props;
	}

	private void setProps(Properties p) {
		FileOutputStream fos = null;
		try {
			// 把config建在files目录�? // fos = activity.openFileOutput(APP_CONFIG,
			// Context.MODE_PRIVATE);

			// 把config建在(自定�?app_config的目录下
			File dirConf = mContext.getDir(APP_CONFIG, Context.MODE_PRIVATE);
			File conf = new File(dirConf, APP_CONFIG);
			fos = new FileOutputStream(conf);

			p.store(fos, null);
			fos.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				fos.close();
			} catch (Exception e) {
			}
		}
	}

	public void set(Properties ps) {
		Properties props = get();
		props.putAll(ps);
		setProps(props);
	}

	public void set(String key, String value) {
		Properties props = get();
		props.setProperty(key, value);
		setProps(props);
	}

	public void remove(String... key) {
		Properties props = get();
		for (String k : key)
			props.remove(k);
		setProps(props);
	}
}
